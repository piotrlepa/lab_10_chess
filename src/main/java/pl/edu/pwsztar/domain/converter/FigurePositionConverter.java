package pl.edu.pwsztar.domain.converter;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.utils.moveChecker.model.FigurePosition;

@Component
public class FigurePositionConverter implements Converter<String, FigurePosition> {
    @Override
    public FigurePosition convert(String from) {
        String[] positions = from.split("_");
        return new FigurePosition(positions[0], positions[1]);
    }
}
