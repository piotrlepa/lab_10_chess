package pl.edu.pwsztar.domain.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface ChessService {

    boolean isCorrectMove(FigureMoveDto figureMoveDto);
}
