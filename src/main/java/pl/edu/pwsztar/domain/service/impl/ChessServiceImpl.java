package pl.edu.pwsztar.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.service.ChessService;
import pl.edu.pwsztar.domain.utils.moveChecker.ChessMoveCheckerManager;
import pl.edu.pwsztar.domain.utils.moveChecker.model.FigurePosition;

@Service
public class ChessServiceImpl implements ChessService {

    private final ChessMoveCheckerManager chessMoveCheckerManager;

    @Autowired
    public ChessServiceImpl(ChessMoveCheckerManager chessMoveCheckerManager) {
        this.chessMoveCheckerManager = chessMoveCheckerManager;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        return chessMoveCheckerManager.isCorrect(figureMoveDto);
    }
}
