package pl.edu.pwsztar.domain.utils.moveChecker.impl;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.constants.ChessConstants;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.domain.utils.moveChecker.ChessMoveChecker;
import pl.edu.pwsztar.domain.utils.moveChecker.model.FigurePosition;

@Component
public class BishopChessMoveChecker implements ChessMoveChecker {

    @Override
    public FigureType getFigureType() {
        return FigureType.BISHOP;
    }

    @Override
    public boolean isCorrect(FigurePosition start, FigurePosition destination) {
        int startHorizontalIndex = ChessConstants.HORIZONTAL_BOARD_IDS.indexOf(start.getHorizontal());
        int destinationHorizontalIndex = ChessConstants.HORIZONTAL_BOARD_IDS.indexOf(destination.getHorizontal());
        int horizontalMoveLength = Math.abs(startHorizontalIndex - destinationHorizontalIndex);

        int startVerticalIndex = ChessConstants.VERTICAL_BOARD_IDS.indexOf(start.getVertical());
        int destinationVerticalIndex = ChessConstants.VERTICAL_BOARD_IDS.indexOf(destination.getVertical());
        int verticalMoveLength = Math.abs(startVerticalIndex - destinationVerticalIndex);

        return horizontalMoveLength == verticalMoveLength;
    }
}
