package pl.edu.pwsztar.domain.utils.moveChecker;

import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.domain.utils.moveChecker.model.FigurePosition;

public interface ChessMoveChecker {

    FigureType getFigureType();

    boolean isCorrect(FigurePosition start, FigurePosition destination);
}
