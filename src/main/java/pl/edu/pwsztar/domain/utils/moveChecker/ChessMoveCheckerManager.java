package pl.edu.pwsztar.domain.utils.moveChecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.domain.utils.moveChecker.model.FigurePosition;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ChessMoveCheckerManager {
    private final Map<FigureType, ChessMoveChecker> factories;
    private final Converter<String, FigurePosition> figurePositionConverter;

    @Autowired
    public ChessMoveCheckerManager(List<ChessMoveChecker> factories, Converter<String, FigurePosition> figurePositionConverter) {
        this.factories = factories.stream()
                .collect(Collectors.toMap(ChessMoveChecker::getFigureType, Function.identity()));
        this.figurePositionConverter = figurePositionConverter;
    }

    public boolean isCorrect(FigureMoveDto figureMoveDto) {
        FigurePosition start = figurePositionConverter.convert(figureMoveDto.getStart());
        FigurePosition destination = figurePositionConverter.convert(figureMoveDto.getDestination());
        if (isStartEqualsDestination(start, destination)) {
            return false;
        } else {
            return ofType(figureMoveDto.getType()).isCorrect(start, destination);
        }
    }

    private ChessMoveChecker ofType(FigureType type) {
        return Optional.ofNullable(factories.get(type))
                .orElseThrow(() -> new IllegalArgumentException("Chess move checker for type " + type + " not found"));
    }

    private boolean isStartEqualsDestination(FigurePosition start, FigurePosition destination) {
        return start.getHorizontal().equals(destination.getHorizontal())
                && start.getVertical().equals(destination.getVertical());
    }
}
