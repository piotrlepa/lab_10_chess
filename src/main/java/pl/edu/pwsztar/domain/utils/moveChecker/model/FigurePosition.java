package pl.edu.pwsztar.domain.utils.moveChecker.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class FigurePosition {
    private final String horizontal;
    private final String vertical;
}
