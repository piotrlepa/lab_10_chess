package pl.edu.pwsztar.domain.constants;

import java.util.Arrays;
import java.util.List;

public class ChessConstants {
    public static final List<String> HORIZONTAL_BOARD_IDS = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h");
    public static final List<String> VERTICAL_BOARD_IDS = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8");
}
